function setAccessToken(token, type) {
  /** @todo: make it more secure way or maybe attach the browser window*/
  sessionStorage.setItem('accessToken', token)
  sessionStorage.setItem('accessTokenType', type)
}

function getAccessToken() {
  /** @todo: make it more secure way or maybe attach the browser window*/
  return {
    value: sessionStorage.getItem('accessToken'),
    type: sessionStorage.getItem('accessTokenType')
  }

}
