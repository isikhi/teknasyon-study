# Teknasyon Case
Teknasyon Case made by Işık (@isikhi)
It uses: NestJs, ExpressJs, PostgreSql, TypeOrm, Redis, SocketIo, Jwt, PassportJs, TS and Ts Based Libraries.

###Why I develop the app with NestJs?
I started developing apps with NestJs three or four months ago. We started to transition because it uses express under it and offers much more manageable, extensible, maintainable code and test writing, and enables modern concepts. I wanted to use it in this project.


## How it works?
- **Please use docker-compose-dev.yml now. Production(docker-compose.yml) is not ready yet.**

- **I uploaded .env file with .env.dist, so you don't need to copy .env at all.**
  
- After setting Docker's ports locally, you can run docker. According to the nest js port you
set, you can see the http / rest-api part of the api document from the **"BASE_DOMAIN/swagger"** address. After the login
BASE_DOMAIN in your browser (example: localhost:3000), you can be directed to the registration and login page. After
registering or login, you will be logged in automatically. (Signup including login processes.)
- After logging in, you will see the list of active users on the server from the "BASE_DOMAIN/user-list.html" page. When
users are online, an alert will appear on the html side, and you will be able to see the user who is online.
There are deficiencies in making offline user and expired/invalid tokens offline.

###Tests
Tests have not been written yet.


### Application development process and what I want to add

I tried to do my best in a short time. Although I don't like the quality of the project,
I think it will be enough to show the development process.

While developing the application, I tried to give a deliverable generic product because of time limitation. I have
waived some standards in this process. In addition, while keeping the online status of the users in the application, I
wanted to keep them in some state manager, redis or nosql database and update these situations with ttl indexes, but
this would be a huge project.

Socket.io is what took me more time while doing the project than anything else I did. The differences in the logic of
working between Socket.io 3 versions and 2 versions were quite time-consuming. Between these versions I wasted more than
usual time with callback firing and reconnection issues about Socket.io. 

