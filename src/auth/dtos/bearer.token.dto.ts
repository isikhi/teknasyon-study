import { UserInfoDto } from "../../user/dtos";
import { Exclude, Expose } from "class-transformer";

export class UserTokenDto extends UserInfoDto {
  @Exclude()
  iat: number;
  @Exclude()
  exp: number;
  @Exclude()
  iss: string;
}