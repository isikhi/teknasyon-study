import { ApiProperty } from '@nestjs/swagger';
import {
  Allow,
  IsString,
  IsEmail,
  IsNotEmpty,
  MinLength,
  MaxLength,
} from 'class-validator';

export class LoginRequestDto {
  @ApiProperty()
  @IsEmail()
  @Allow()
  email: string;

  @ApiProperty()
  @IsString()
  @Allow()
  @MinLength(8)
  @MaxLength(128)
  password: string;
}

const ONE_DAY_IN_MS = 24 * 60 * 60 * 1000

export class LoginResponseDto {
  @ApiProperty()
  token: string;

  @ApiProperty({ default: 'Bearer' })
  type: string;
  @ApiProperty({ default: ONE_DAY_IN_MS, description: 'Expires in default 1 day.' })
  expiresIn: number;
}
