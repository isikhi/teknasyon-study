import { ApiProperty } from '@nestjs/swagger';
import {
  Allow,
  IsString,
  IsEmail,
  IsNotEmpty,
  MinLength,
  MaxLength, IsISO31661Alpha2, IsLocale,
} from 'class-validator';
import { UserInfoDto } from '../../user/dtos';
import { Transform } from "class-transformer";

export class CreateUserRequestDto {
  @ApiProperty()
  @IsString()
  @Allow()
  @MinLength(1)
  @MaxLength(128)
  name: string;

  @ApiProperty()
  @IsEmail()
  @Allow()
  email: string;

  @ApiProperty()
  @IsString()
  @Allow()
  @IsNotEmpty()
  @MinLength(8)
  @MaxLength(128)
  password: string;

  @ApiProperty()
  @IsString()
  @Allow()
  @IsISO31661Alpha2()
  @Transform(({ value }) => value.toString().toLowerCase())
  country: string;

  @ApiProperty()
  @Allow()
  @IsString()
  @IsLocale()
  @Transform(({ value }) => (value.toString().toLowerCase()))
  lang: string;
}

export class CreatedUserResponseDto extends UserInfoDto {
}
