import { HttpException, HttpStatus, Injectable, Logger } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { UserEntity } from '../../shared/Entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import bcrypt from 'bcryptjs';
import {
  CreateUserRequestDto,
  LoginRequestDto,
  LoginResponseDto,
} from '../dtos';
import { plainToClass } from 'class-transformer';
import { ConfigService } from '@nestjs/config';
import { UserInfoDto } from '../../user/dtos';
import { errorHandler } from '../../shared/Helpers/error.http.exception.helper';
import { IJWTSignPayload } from '../schemas/auth.user.interface';
import { MessageService } from "../../event/services/message.service";

@Injectable()
export class AuthService {
  private readonly logger: Logger = new Logger('AuthService')

  constructor(
    private readonly jwtService: JwtService,
    @InjectRepository(UserEntity)
    private readonly authRepository: Repository<UserEntity>,
    private readonly configService: ConfigService,
    private readonly messageService: MessageService,
  ) {
  }

  static async comparePasswords(
    hashedPassword: string,
    plainPassword: string,
  ): Promise<boolean> {
    return bcrypt.compare(plainPassword, hashedPassword);
  }

  private static getSignPayload(user: UserInfoDto): IJWTSignPayload {
    return {
      ...user,
    };
  }

  async signToken(payload: IJWTSignPayload): Promise<LoginResponseDto> {
    const token = await this.jwtService.sign(payload, {
      issuer: this.configService.get('JWT_ISSUER'),
    });
    const { exp } = await this.jwtService.verifyAsync(token);
    return {
      token,
      type: 'Bearer',
      expiresIn: exp,
    };
  }

  async signup(user: CreateUserRequestDto): Promise<LoginResponseDto> {
    try {
      const userFound = await this.authRepository.findOne({
        email: user.email,
      });
      if (userFound) {
        throw new HttpException('User already exists', HttpStatus.CONFLICT);
      }

      const newUser = await this.authRepository.save(
        this.authRepository.create(user),
      );
      this.logger.log(`User registered, ${user}`)
      this.messageService.signup(newUser.id);
      const payloadWillBeSign = AuthService.getSignPayload(newUser);
      const token = await this.signToken(payloadWillBeSign);
      return plainToClass(LoginResponseDto, token);
    } catch (err) {
      throw errorHandler(err, AuthService.name);
    }
  }

  async login(body: LoginRequestDto): Promise<LoginResponseDto> {
    try {
      const user = await this.authRepository.findOne({ email: body.email });
      if (!user) {
        throw new HttpException('User not found', HttpStatus.NOT_FOUND);
      }
      const passwordsMatch = await AuthService.comparePasswords(
        user.password,
        body.password,
      );
      if (user && passwordsMatch) {
        const payloadWillBeSign = AuthService.getSignPayload(user);
        const token = await this.signToken(payloadWillBeSign);
        this.messageService.login(user.id);
        await this.authRepository.update({
          id: user.id,
        }, { online: true });
        return plainToClass(LoginResponseDto, token);
      }
      throw new HttpException(
        'Please check your information',
        HttpStatus.BAD_REQUEST,
      );
    } catch (err) {
      throw errorHandler(err, AuthService.name);
    }
  }
}
