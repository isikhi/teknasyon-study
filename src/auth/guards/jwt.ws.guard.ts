import { AuthGuard } from '@nestjs/passport';
import { Injectable } from '@nestjs/common';

@Injectable()
export class JwtWsGuard extends AuthGuard('jwt-ws') {
}
