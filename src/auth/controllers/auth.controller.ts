import {
  Controller,
  Post,
  Body,
  UsePipes,
  ValidationPipe, UseInterceptors,
} from '@nestjs/common';
import { ApiTags, ApiBearerAuth, ApiOperation, ApiCreatedResponse, ApiBadRequestResponse } from '@nestjs/swagger';
import { RateLimiterInterceptor } from 'nestjs-rate-limiter';

import { AuthService } from '../services/auth.service';
import { LoginRequestDto, LoginResponseDto, CreateUserRequestDto } from '../dtos';
import { ValidationErrorResponse } from "../../shared/Helpers/error.validation.response";

@ApiTags('Authentication')
@Controller('auth')
@UseInterceptors(RateLimiterInterceptor)
export class AuthController {
  constructor(private readonly authService: AuthService) {
  }

  @ApiCreatedResponse({ type: LoginResponseDto })
  @ApiBadRequestResponse({ type: ValidationErrorResponse })
  @UsePipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
      forbidUnknownValues: true,
      transformOptions: {
        enableImplicitConversion: true,
      },
    })
  )
  @Post('login')
  async login(@Body() body: LoginRequestDto): Promise<LoginResponseDto> {
    return this.authService.login(body);
  }

  @ApiCreatedResponse({ type: LoginResponseDto })
  @ApiBadRequestResponse({ type: ValidationErrorResponse })
  @UsePipes(
    new ValidationPipe({
      whitelist: true,
      transform: true,
      forbidUnknownValues: true,
      transformOptions: {
        enableImplicitConversion: true,
      },
    })
  )
  @Post('signup')
  signUp(@Body() body: CreateUserRequestDto): Promise<LoginResponseDto> {
    return this.authService.signup(body)
  }
}
