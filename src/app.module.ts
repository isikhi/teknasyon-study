import path from 'path'
import { Module } from '@nestjs/common';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { TypeOrmModule } from '@nestjs/typeorm';
import { EventEmitterModule } from '@nestjs/event-emitter';
import { EventModule } from './event/event.module';
import { SocketModule } from './socket/socket.module';
import { AuthModule } from './auth/auth.module';
import { ServeStaticModule } from '@nestjs/serve-static';
import { UserModule } from "./user/user.module";


@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    TypeOrmModule.forRootAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: async (configService: ConfigService) => {
        return {
          type: 'postgres',
          host: configService.get('POSTGRES_HOST'),
          port: parseInt(configService.get('POSTGRES_PORT')),
          username: configService.get('POSTGRES_USER'),
          password: configService.get('POSTGRES_PASSWORD'),
          database: configService.get('POSTGRES_DB'),
          entities: [path.join(__dirname, '**', '*.entity.{js,ts}'), path.join(__dirname, '..', '**', '*.entity.{js,ts}')],
          synchronize: true,
        }
      }
    }),
    EventEmitterModule.forRoot(),
    AuthModule,
    UserModule,
    EventModule,
    SocketModule,
    ServeStaticModule.forRoot({
      renderPath: '/',
      rootPath: path.join(__dirname, '..', '..', 'public'),
    }),
  ],
  controllers: [],
  providers: [],
})

export class AppModule {
}
