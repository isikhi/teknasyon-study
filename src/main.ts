import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { Transport } from '@nestjs/microservices';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { version } from '../package.json';
import { IoAdapter } from '@nestjs/platform-socket.io';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  app.enableCors();
  app.useWebSocketAdapter(new IoAdapter(app));
  app.connectMicroservice({
    transport: Transport.REDIS,
    options: {
      url: process.env.REDIS_URI,
    },
  });

  const options = new DocumentBuilder()
    .addBearerAuth()
    .setTitle(process.env.APP_NAME)
    .setVersion(version)
    .addBearerAuth({ bearerFormat: 'jwt', scheme: 'bearer', type: 'http' })
    .build();
  const document = SwaggerModule.createDocument(app, options);
  SwaggerModule.setup('swagger', app, document);

  await app.startAllMicroservicesAsync();

  const PORT = parseInt(process.env.PORT) || 3000;

  await app.listen(PORT);
  console.log(`${process.env.APP_NAME} is running on: ${await app.getUrl()} with ${process.env.NODE_ENV} environment`);
}

bootstrap();

