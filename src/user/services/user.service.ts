import {
  HttpException, HttpStatus,
  Injectable,
} from '@nestjs/common';
import { UserEntity } from '../../shared/Entities/user.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { plainToClass } from 'class-transformer';
import { UserInfoDto } from "../dtos";
import { errorHandler } from "../../shared/Helpers/error.http.exception.helper";

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
  ) {
  }

  async findUserById(id): Promise<UserInfoDto> {
    try {
      const user = await this.userRepository.findOne({ id });
      if (!user) {
        throw new HttpException('User not found', HttpStatus.NOT_FOUND);
      }
      return plainToClass(UserInfoDto, user)
    } catch (e) {
      throw errorHandler(e, UserService.name)
    }
  }

  async findUserByEmail(email): Promise<UserInfoDto> {
    try {
      const user = await this.userRepository.findOne({ email });
      if (!user) {
        throw new HttpException('User not found', HttpStatus.NOT_FOUND);
      }
      return plainToClass(UserInfoDto, user)
    } catch (e) {
      throw errorHandler(e, UserService.name)
    }
  }

  async findOnlineUsers(val: boolean = true): Promise<UserInfoDto[]> {
    try {
      const users = await this.userRepository.find({ online: val });
      if (!users || !users.length) {
        throw new HttpException('User not found', HttpStatus.NOT_FOUND);
      }
      return plainToClass(UserInfoDto, users)
    } catch (e) {
      throw errorHandler(e, UserService.name);
    }
  }

  async setUserOnlineStatus(searchCondition, val: boolean): Promise<void> {
    try {
      await this.userRepository.update(searchCondition, { online: val });
    } catch (e) {
      throw errorHandler(e, UserService.name);
    }
  }
}
