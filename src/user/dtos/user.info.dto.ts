import { ApiProperty } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';
import { Allow, IsAlphanumeric } from 'class-validator';

export class UserInfoDto {
  @ApiProperty()
  id: number

  @ApiProperty()
  @Allow()
  name: string;

  @ApiProperty()
  @Allow()
  email: string;

  @Exclude()
  @IsAlphanumeric()
  password: string;

  @ApiProperty()
  @Allow()
  country: string;

  @ApiProperty()
  @Allow()
  lang: string;
}
