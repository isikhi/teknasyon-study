import { UserInfoDto } from './user.info.dto';
import { ApiProperty } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';
import {
  Allow,
  IsString,
  IsEmail,
  IsNotEmpty,
  MinLength,
  MaxLength,
} from 'class-validator';

export class UserLoginRequestDto {
  @ApiProperty()
  @IsEmail()
  @Allow()
  email: string;

  @ApiProperty()
  @IsString()
  @Allow()
  @IsNotEmpty()
  @MinLength(1)
  @MaxLength(128)
  password: string;
}

// @ApiProperty()
// @IsString()
// @Allow()
// @MinLength(1)
// @MaxLength(128)
// name: string;
//
export class UserLoginDtoResponseDto extends UserInfoDto {
}
