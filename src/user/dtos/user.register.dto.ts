import { UserInfoDto } from './user.info.dto';

export class CreateUserRequestDto extends UserInfoDto {
}

export class CreateUserResponseDto extends UserInfoDto {
}
