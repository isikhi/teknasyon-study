import { Controller, Get, Param, ParseBoolPipe, Query, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { User } from '../../shared/Decarators/user.decorator';
import { UserService } from '../services/user.service';
import { JwtGuard } from "../../auth/guards/jwt.guard";
import { ApiBearerAuth, ApiOkResponse, ApiParam, ApiQuery, ApiTags, ApiUnauthorizedResponse } from "@nestjs/swagger";
import { UserInfoDto } from "../dtos";
import { query } from "express";
import { isBoolean } from "class-validator";

@ApiTags('User')
@UseGuards(JwtGuard)
@ApiBearerAuth()
@Controller('user')
export class UserController {
  constructor(private readonly userService: UserService) {
  }

  @Get('/search')
  @ApiOkResponse({ type: [UserInfoDto] })
  @ApiUnauthorizedResponse()
  @ApiQuery({
    name: 'online',
    enum: ['true', 'false']
  })
  getOnlineUsers(
    @Query('online', ParseBoolPipe) online: boolean
  ): Promise<UserInfoDto[]> {
    return this.userService.findOnlineUsers(online)
  }

  @Get()
  @ApiOkResponse({ type: UserInfoDto })
  @ApiUnauthorizedResponse()
  getMe(
    @User() user,
  ): Promise<UserInfoDto> {
    return user;
  }

  @Get(':id')
  @ApiParam({
    required: false,
    name: 'id',
  })
  @ApiOkResponse({ type: UserInfoDto })
  @ApiUnauthorizedResponse()
  getUser(
    @Param('id') id: string
  ): Promise<UserInfoDto> {
    return this.userService.findUserById(id)
  }

}
