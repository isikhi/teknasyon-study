import { ApiProperty } from '@nestjs/swagger';
import { Exclude } from 'class-transformer';
import { Allow, IsAlphanumeric } from 'class-validator';

export class UserEventTransportDto {
  id: number
  name: string;
  email: string;
}