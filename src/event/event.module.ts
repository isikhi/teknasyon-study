import { Module } from '@nestjs/common';
import { MessageService } from './services/message.service';
import { ClientsModule, Transport } from '@nestjs/microservices';
import { ConfigModule, ConfigService } from '@nestjs/config';
import { MessageController } from './controllers/message.controller';
import { ServiceNames } from '../shared/Constants';
import { EventService } from "./services/event.service";

@Module({
  imports: [
    ClientsModule.registerAsync([
      {
        name: ServiceNames.REDIS,
        imports: [ConfigModule],
        inject: [ConfigService],
        useFactory: (configService: ConfigService) => {
          return {
            transport: Transport.REDIS,
            options: {
              url: configService.get('REDIS_URI'),
            },
          };
        },
      },
    ]),
  ],
  providers: [MessageService, EventService],
  controllers: [MessageController],
  exports: [MessageService]
})
export class EventModule {
}
