import { Injectable } from '@nestjs/common';
import { EventEmitter2 } from '@nestjs/event-emitter';

@Injectable()
export class EventService {
  constructor(
    private eventEmitter: EventEmitter2
  ) {
  }

  signup(id) {
    this.eventEmitter.emit('signup', id)
  }

  login(id) {
    this.eventEmitter.emit('login', id)
  }
}
