import { Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { ServiceNames } from '../../shared/Constants';
import { Observable } from "rxjs";

@Injectable()
export class MessageService {
  constructor(
    @Inject(ServiceNames.REDIS) private readonly redisProxy: ClientProxy,
  ) {
  }

  signup(id): Observable<any> {
    return this.redisProxy.emit('signup', id)
  }

  login(id): Observable<any> {
    return this.redisProxy.emit('login', id);
  }

  logout(id): Observable<any> {
    return this.redisProxy.emit('logout', id);
  }
}
