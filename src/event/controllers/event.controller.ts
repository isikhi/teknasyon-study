import { Controller } from '@nestjs/common';
import { EventPattern, MessagePattern, Payload } from '@nestjs/microservices';
import { MessageService } from '../services/message.service';
import { EventService } from "../services/event.service";

@Controller('message')
export class MessageController {
  constructor(
    private readonly messageService: MessageService,
    private readonly eventService: EventService,
  ) {
  }

  @EventPattern('signup')
  signup(@Payload() data: any) {

  }

  @EventPattern('login')
  login(@Payload() data: any) {

  }

  @EventPattern('logout')
  logout(@Payload() data: any) {

  }


}
