import { Controller } from '@nestjs/common';
import { MessagePattern, Payload } from '@nestjs/microservices';
import { MessageService } from '../services/message.service';
import { EventService } from "../services/event.service";

@Controller('message')
export class MessageController {
  constructor(
    private readonly messageService: MessageService,
    private readonly eventService: EventService,
  ) {}

  @MessagePattern('signup')
  signup(@Payload() data: any) {
    this.eventService.signup(data);
  }

  @MessagePattern('login')
  login(@Payload() data: any) {
    this.eventService.login(data);
  }
}
