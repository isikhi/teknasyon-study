import { Module } from '@nestjs/common';

import { SocketGateway } from './socket.gateway';
import { JwtModule } from "@nestjs/jwt";
import { ConfigModule, ConfigService } from "@nestjs/config";
import { EventModule } from "../event/event.module";
import { UserModule } from "../user/user.module";

@Module({
  imports: [
    JwtModule.registerAsync({
      imports: [ConfigModule],
      inject: [ConfigService],
      useFactory: (configService: ConfigService) => {
        return {
          secret: configService.get('JWT_SECRET'),
          signOptions: {
            expiresIn: configService.get('JWT_EXPIRE_TIME'),
          },
        };
      },
    }),
    EventModule,
    UserModule
  ],
  providers: [SocketGateway],
  exports: [SocketGateway],
  controllers: [],
})
export class SocketModule {
}