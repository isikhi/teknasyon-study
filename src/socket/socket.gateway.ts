import {
  SubscribeMessage,
  WebSocketGateway,
  OnGatewayInit,
  WebSocketServer,
  OnGatewayConnection,
  OnGatewayDisconnect,
} from '@nestjs/websockets';
import { Logger, UseGuards } from '@nestjs/common';
import { Socket, Server } from 'socket.io';
import { JwtWsGuard } from "../auth/guards/jwt.ws.guard";
import { extractAuthenticationHeaderFromSocketHandshake } from "../shared/Helpers/socket.authentication";
import { JwtService } from "@nestjs/jwt";
import { UserInfoDto } from "../user/dtos";
import { plainToClass } from "class-transformer";
import { MessageService } from "../event/services/message.service";
import { OnEvent } from "@nestjs/event-emitter";
import { UserService } from "../user/services/user.service";

@WebSocketGateway({ namespace: 'online-users' })
export class SocketGateway implements OnGatewayInit, OnGatewayConnection, OnGatewayDisconnect {
  private activeConnections = new Map();

  constructor(
    private readonly jwtService: JwtService,
    private readonly messageService: MessageService,
    private readonly userService: UserService,
  ) {
  }

  @WebSocketServer() server: Server;
  private logger: Logger = new Logger('AppGateway');

  afterInit(server: Server) {
    this.logger.log('Ws Initialized.');
  }

  async handleConnection(client: Socket, ...args: any[]) {
    const token = extractAuthenticationHeaderFromSocketHandshake(client.handshake)
    if (!token) {
      return client.disconnect()
    }
    const rawUser = this.jwtService.decode(token, { json: true })
    if (!rawUser) return client.disconnect();
    const user = plainToClass(UserInfoDto, rawUser)
    this.logger.log(`Client connected: ${client.id}, UserId: ${user.id}`);
    this.activeConnections.set(client, user.id)
    await this.userService.setUserOnlineStatus({id: user.id}, true)
    this.messageService.login(user.id)
  }

  @OnEvent('signup') handleSignup(id) {
    this.server.emit('signup', id)
  }

  @OnEvent('login') handleLogin(id) {
    this.server.emit('login', { id })
  }

  // @OnEvent('logout') handleLogout(id) {
  //   this.activeConnections.delete(id);
  // }

  public async handleDisconnect(client: Socket) {
    const userId = this.activeConnections.get(client);
    this.logger.log(`Client disconnected: ${client.id}`);
    this.messageService.logout(client.id)
    await this.userService.setUserOnlineStatus({id: userId}, false)
  }
}
