import { ApiProperty } from "@nestjs/swagger";
import { ValidationError } from "class-validator";

export class ValidationErrorResponse implements ValidationError {
  @ApiProperty()
  readonly statusCode;

  @ApiProperty()
  readonly message: ValidationError[];

  @ApiProperty()
  error: any;

  @ApiProperty({ nullable: true })
  children: ValidationError[];

  @ApiProperty({ nullable: true })
  property: string;
}
