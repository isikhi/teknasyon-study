import {
  HttpStatus,
  InternalServerErrorException,
  Logger,
} from '@nestjs/common';

export const errorHandler = (err, ctx) => {
  if (err.status) {
    if (
      err.status > HttpStatus.AMBIGUOUS &&
      err.status < HttpStatus.INTERNAL_SERVER_ERROR
    )
      return err;
  }
  Logger.error(err.message, err.trace, ctx || err.context);
  return new InternalServerErrorException(
    'Oops! An unexpected situation happened. We have informed about the situation, please try again later. ',
  );
};

