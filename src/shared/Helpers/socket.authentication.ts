export function extractAuthenticationHeaderFromSocketHandshake(handshake) {
  try {
    if (!handshake || !handshake.query || !handshake.query.Authorization) {
      return null
    }
    const { type, value } = JSON.parse(handshake.query.Authorization)
    return value
  } catch (e) {
    return null;
  }
}