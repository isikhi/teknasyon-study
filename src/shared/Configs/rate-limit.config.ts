import { ConfigService } from "@nestjs/config";
import { NodeEnvironments } from "../Constants";

export const RateLimitWillBeAppliedEnvironments = [NodeEnvironments.PRODUCTION]

export function getRateLimitConfig(environment) {
  const baseRateLimitConfig = {
    points: 2,
    duration: 300,
    customResponseSchema: rateLimiterResponse => ({
      message: 'You have reached the limit of login requests. You have to wait 5 minutes before trying again.',
      statusCode: 429,
      error: 'Too Many Request'
    }),
    for: 'Express',

  }
  return RateLimitWillBeAppliedEnvironments.includes(environment) ? baseRateLimitConfig : {
    ...baseRateLimitConfig,
    points: Number.MAX_SAFE_INTEGER
  }

}