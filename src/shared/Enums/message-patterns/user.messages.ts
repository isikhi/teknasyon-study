export enum UserMessages {
  REGISTER = 'register',
  LOGIN = 'login',
  LOGOUT = 'logout',
}
