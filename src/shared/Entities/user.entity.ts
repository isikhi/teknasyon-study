import {
  BaseEntity,
  Entity,
  PrimaryGeneratedColumn,
  Column,
  BeforeInsert,
} from 'typeorm';
import * as bcrypt from 'bcryptjs';

@Entity()
export class UserEntity extends BaseEntity {
  @PrimaryGeneratedColumn()
  id: number;

  @Column()
  name: string;

  @Column({ unique: true })
  email: string;

  @Column()
  lang: string;

  @Column()
  country: string;

  @Column()
  password: string;

  @Column()
  online: boolean

  @BeforeInsert()
  async setPreSaveRequirements() {
    this.password = await bcrypt.hash(
      this.password,
      process.env.JWT_HASH_SALT || 10,
    );
    this.online = true;
  }

}
