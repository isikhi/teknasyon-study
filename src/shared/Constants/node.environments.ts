export const enum NodeEnvironments {
  PRODUCTION = 'production',
  DEVELOPMENT = 'development',
  TEST = 'test'
}
